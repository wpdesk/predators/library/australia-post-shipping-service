<?php

namespace WPDesk\AustraliaPostShippingService;

/**
 * A class that defines Australia Post services.
 */
class AustraliaPostServices
{

    /**
     * @return array
     */
    private function get_services(): array
    {
        return [
            'domestic_au' => [
                // Domestic - Parcel
                'AUS_PARCEL_REGULAR' => __('Australia Post Parcel Post', 'australia-post-shipping-service'),
                'AUS_PARCEL_EXPRESS' => __('Australia Post Express Post', 'australia-post-shipping-service'),
                /*
                                'AUS_PARCEL_REGULAR_SATCHEL_500G' => __( 'Australia Post Parcel Post Small Satchel', 'australia-post-shipping-service' ),
                                'AUS_PARCEL_REGULAR_SATCHEL_3KG' => __( 'Australia Post Parcel Post Small Satchel', 'australia-post-shipping-service' ),
                                'AUS_PARCEL_REGULAR_SATCHEL_5KG' => __( 'Australia Post Parcel Post Small Satchel', 'australia-post-shipping-service' ),
                                'AUS_PARCEL_EXPRESS_SATCHEL_500G' => __( 'Australia Post Express Post Small Satchel', 'australia-post-shipping-service' ),
                                'AUS_PARCEL_EXPRESS_SATCHEL_3KG' => __( 'Australia Post Express Post Medium (3Kg) Satchel', 'australia-post-shipping-service' ),
                                'AUS_PARCEL_EXPRESS_SATCHEL_5KG' => __( 'Australia Post Express Post Large (5Kg) Satchel', 'australia-post-shipping-service' ),
                                'AUS_PARCEL_COURIER' => __( 'Australia Post Courier Post', 'australia-post-shipping-service' ),
                                'AUS_PARCEL_COURIER_SATCHEL_MEDIUM' => __( 'Australia Post Courier Post Assessed Medium Satchel', 'australia-post-shipping-service' ),
                */
                /*
                                // Domestic - Letter
                                'AUS_LETTER_REGULAR_SMALL' => __( 'Australia Post Letter Regular Small', 'australia-post-shipping-service' ),
                                'AUS_LETTER_REGULAR_MEDIUM' => __( 'Australia Post Letter Regular Medium', 'australia-post-shipping-service' ),
                                'AUS_LETTER_REGULAR_LARGE' => __( 'Australia Post Letter Regular Large', 'australia-post-shipping-service' ),
                                'AUS_LETTER_REGULAR_LARGE_125' => __( 'Australia Post Letter Regular Large (125g)', 'australia-post-shipping-service' ),
                                'AUS_LETTER_REGULAR_LARGE_250' => __( 'Australia Post Letter Regular Large (250g)', 'australia-post-shipping-service' ),
                                'AUS_LETTER_REGULAR_LARGE_500' => __( 'Australia Post Letter Regular Large (500g)', 'australia-post-shipping-service' ),
                                'AUS_LETTER_EXPRESS_SMALL' => __( 'Australia Post Letter Express Small', 'australia-post-shipping-service' ),
                                'AUS_LETTER_EXPRESS_MEDIUM' => __( 'Australia Post Letter Express Medium', 'australia-post-shipping-service' ),
                                'AUS_LETTER_EXPRESS_LARGE' => __( 'Australia Post Letter Express Large', 'australia-post-shipping-service' ),
                                'AUS_LETTER_EXPRESS_LARGE_125' => __( 'Australia Post Letter Express Large (125g)', 'australia-post-shipping-service' ),
                                'AUS_LETTER_EXPRESS_LARGE_250' => __( 'Australia Post Letter Express Large (250g)', 'australia-post-shipping-service' ),
                                'AUS_LETTER_EXPRESS_LARGE_500' => __( 'Australia Post Letter Express Large (500g)', 'australia-post-shipping-service' ),
                                'AUS_LETTER_PRIORITY_SMALL' => __( 'Australia Post Letter Priority Small', 'australia-post-shipping-service' ),
                                'AUS_LETTER_PRIORITY_MEDIUM' => __( 'Australia Post Letter Priority Medium', 'australia-post-shipping-service' ),
                                'AUS_LETTER_PRIORITY_LARGE' => __( 'Australia Post Letter Priority Large', 'australia-post-shipping-service' ),
                                'AUS_LETTER_PRIORITY_LARGE_125' => __( 'Australia Post Letter Priority Large (125g)', 'australia-post-shipping-service' ),
                                'AUS_LETTER_PRIORITY_LARGE_250' => __( 'Australia Post Letter Priority Large (250g)', 'australia-post-shipping-service' ),
                                'AUS_LETTER_PRIORITY_LARGE_500' => __( 'Australia Post Letter Priority Large (500g)', 'australia-post-shipping-service' ),
                */
            ],
            'international' => [
                // International - Parcel
                'INT_PARCEL_STD_OWN_PACKAGING' => __('Australia Post International Standard', 'australia-post-shipping-service'),
                'INT_PARCEL_EXP_OWN_PACKAGING' => __('Australia Post International Express', 'australia-post-shipping-service'),
                /*
                                'INT_PARCEL_COR_OWN_PACKAGING' => __( 'Australia Post International Courier', 'australia-post-shipping-service' ),
                                'INT_PARCEL_AIR_OWN_PACKAGING' => __( 'Australia Post International Economy Air', 'australia-post-shipping-service' ),
                                'INT_PARCEL_SEA_OWN_PACKAGING' => __( 'Australia Post International Economy Sea', 'australia-post-shipping-service' ),
                */
                /*
                                // International - Letter
                                'INT_LETTER_REG_SMALL_ENVELOPE' => __( 'Australia Post International Letter DL', 'australia-post-shipping-service' ),
                                'INT_LETTER_REG_LARGE_ENVELOPE' => __( 'Australia Post International Letter B4', 'australia-post-shipping-service' ),
                                'INT_LETTER_EXP_OWN_PACKAGING' => __( 'Australia Post International Letter Express', 'australia-post-shipping-service' ),
                                'INT_LETTER_COR_OWN_PACKAGING' => __( 'Australia Post International Letter Courier', 'australia-post-shipping-service' ),
                                'INT_LETTER_AIR_OWN_PACKAGING_LIGHT' => __( 'Australia Post International Letter Air Light', 'australia-post-shipping-service' ),
                                'INT_LETTER_AIR_OWN_PACKAGING_MEDIUM' => __( 'Australia Post International Letter Air Medium', 'australia-post-shipping-service' ),
                                'INT_LETTER_AIR_OWN_PACKAGING_HEAVY' => __( 'Australia Post International Letter Air Heavy', 'australia-post-shipping-service' ),
                */
            ],
        ];
    }

    public function get_all_services()
    {
        return array_merge($this->get_services_domestic_au(), $this->get_services_international());
    }

    /**
     * @return array
     */
    public function get_services_domestic_au(): array
    {
        return $this->get_services()['domestic_au'];
    }

    /**
     * @return array
     */
    public function get_services_international(): array
    {
        return $this->get_services()['international'];
    }

}
