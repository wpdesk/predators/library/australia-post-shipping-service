<?php

namespace WPDesk\AustraliaPostShippingService\Api;

use Fontis\Auspost\Api\Postage\Domestic\Letter\Services\GetServicesParams;
use Fontis\Auspost\Api\Postage\Domestic\Parcel\Cost\CalculationParams;
use Fontis\Auspost\Exception\EndpointServiceError;
use Fontis\Auspost\Model\Postage\Enum\ServiceCode;
use http\Exception\RuntimeException;
use Psr\Log\LoggerInterface;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\AbstractShipping\Settings\SettingsValuesAsArray;
use WPDesk\AustraliaPostShippingService\AustraliaPostSettingsDefinition;
use WPDesk\AustraliaPostShippingService\AustraliaPostShippingService;
use WPDesk\AustraliaPostShippingService\Exception\ApiResponseException;

/**
 * Can check connection.
 */
class ConnectionChecker
{

    /**
     * Settings.
     *
     * @var SettingsValuesAsArray
     */
    private $settings;

    /**
     * Logger.
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ConnectionChecker constructor.
     *
     * @param SettingsValues $settings .
     * @param LoggerInterface $logger .
     */
    public function __construct(SettingsValues $settings, LoggerInterface $logger)
    {
        $this->settings = $settings;
        $this->logger = $logger;
    }

    /**
     * Pings API.
     *
     * @throws \Exception .
     */
    public function check_connection()
    {

        $this->logger->debug('Connection checker', ['source' => 'australiapost']);
        $auspost = Auspost::create_with_logger($this->settings->get_value(AustraliaPostSettingsDefinition::API_KEY, AustraliaPostSettingsDefinition::DEFAULT_API_KEY), $this->logger);
        try {
            $result = $auspost->postage()->calculateDomesticParcelPostage(new CalculationParams(
                3000,
                3011,
                10,
                10,
                10,
                10,
                ServiceCode::AUS_PARCEL_REGULAR
            ));
            $this->logger->debug('Connection success', ['source' => 'australiapost', 'rates' => $result]);
        } catch (EndpointServiceError $ese) {
            $message = $auspost->getMessageFromException($ese);
            $this->logger->debug(' Connection checker error', ['source' => 'australiapost', 'error' => $message]);
            throw new ApiResponseException($message);
        } catch (\Exception $e) {
            $this->logger->debug(' Connection checker error', ['source' => 'australiapost', 'error' => $e->getMessage()]);
            throw $e;
        }
    }

}
