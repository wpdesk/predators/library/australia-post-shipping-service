<?php

namespace WPDesk\AustraliaPostShippingService\Api;

use Http\Client\Common\Plugin\AddHostPlugin;
use Http\Client\Common\Plugin\HeaderDefaultsPlugin;
use Http\Client\Common\PluginClient;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\UriFactoryDiscovery;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use WPDesk\AustraliaPostShippingService\Http\LoggerPlugin;

/**
 * Australia Post API Configuration.
 */
class Configurator extends \Fontis\Auspost\HttpClient\Configurator implements LoggerAwareInterface
{

    use LoggerAwareTrait;

    public function createHttpClient()
    {
        $client = HttpClientDiscovery::find();

        $plugins = [
            new AddHostPlugin(UriFactoryDiscovery::find()->createUri($this->getEndpoint())),
            new HeaderDefaultsPlugin([
                'auth-key' => $this->getApiKey(),
            ]),
            new LoggerPlugin($this->logger)
        ];

        return new PluginClient($client, $plugins);

    }

}
