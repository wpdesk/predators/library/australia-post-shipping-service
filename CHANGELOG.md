## [1.1.0] - 2022-08-09
### Added
- en_AU translators

## [1.0.6] - 2022-05-23
### Added
- GST
### Fixed
- texts

## [1.0.2] - 2022-05-19
### Fixed
- links

## [1.0.1] - 2022-05-19
### Fixed
- default package weight

## [1.0.0] - 2022-05-16
### Added
- initial version
